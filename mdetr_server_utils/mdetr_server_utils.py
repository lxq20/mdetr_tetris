import socket
import sys
import _thread
import cv2,os
import numpy as np
import datetime

from numpy.core.shape_base import vstack
import random
import torch
from PIL import Image
import requests
import torchvision.transforms as T
import matplotlib.pyplot as plt
from collections import defaultdict
import torch.nn.functional as F
import numpy as np
from skimage.measure import find_contours

from matplotlib import patches,  lines
from matplotlib.patches import Polygon
from models import build_model
from models.postprocessors import build_postprocessors

torch.set_grad_enabled(False)


class Block():
    def __init__(self,color,shape,name,size_x,size_y,size_z):
        self.color = color
        self.shape = shape
        self.name = name
        self.size_x = size_x
        self.size_y = size_y
        self.size_z = size_z
    def get_attr(self):
        return [self.color,self.shape][random.randint(0,1)]
obj1=Block("pink","L shape","obj1",None,None,None)
obj2=Block("dark blue","square","obj2",20,10,20)
obj3=Block("turquoise","U shape","obj3",None,None,None)
obj4=Block("red","long cuboid","obj4",40,10,10)
obj5=Block("purple","short cuboid","obj5",20,10,10)
obj6=Block("aquamarine","Z shape","obj6",None,None,None)
obj7=Block("yellow","short L shape","obj7",None,None,None)
obj8=Block("white","T shape","obj8",None,None,None)
block_list=[obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8]

# standard PyTorch mean-std input image normalization
transform = T.Compose([
    T.Resize(800),
    T.ToTensor(),
    T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])

# for output bounding box post-processing
def box_cxcywh_to_xyxy(x):
    x_c, y_c, w, h = x.unbind(1)
    b = [(x_c - 0.5 * w), (y_c - 0.5 * h),
         (x_c + 0.5 * w), (y_c + 0.5 * h)]
    return torch.stack(b, dim=1)

def rescale_bboxes(out_bbox, size):
    img_w, img_h = size
    b = box_cxcywh_to_xyxy(out_bbox)
    b = b * torch.tensor([img_w, img_h, img_w, img_h], dtype=torch.float32)
    return b

# colors for visualization
COLORS = [[0.000, 0.447, 0.741], [0.850, 0.325, 0.098], [0.929, 0.694, 0.125],
          [0.494, 0.184, 0.556], [0.466, 0.674, 0.188], [0.301, 0.745, 0.933]]

def apply_mask(image, mask, color, alpha=0.5):
    """Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                  image[:, :, c] *
                                  (1 - alpha) + alpha * color[c] * 255,
                                  image[:, :, c])
    return image

def plot_results(pil_img, scores, boxes, labels,caption, masks=None):
    plt.figure(figsize=(16,10))
    np_image = np.array(pil_img)
    ax = plt.gca()
    colors = COLORS * 100
    if masks is None:
      masks = [None for _ in range(len(scores))]

    assert len(scores) == len(boxes) == len(labels) == len(masks)
    for s, (xmin, ymin, xmax, ymax), l, mask, c in zip(scores, boxes.tolist(), labels, masks, colors):
        ax.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
                                   fill=False, color=c, linewidth=3))
        text = f'{l}: {s:0.2f}'
        ax.text(xmin, ymin, text, fontsize=15, bbox=dict(facecolor='white', alpha=0.8))

        if mask is None:
          continue
        np_image = apply_mask(np_image, mask, c)

        padded_mask = np.zeros((mask.shape[0] + 2, mask.shape[1] + 2), dtype=np.uint8)
        padded_mask[1:-1, 1:-1] = mask
        contours = find_contours(padded_mask, 0.5)
        for verts in contours:
          # Subtract the padding and flip (y, x) to (x, y)
          verts = np.fliplr(verts) - 1
          p = Polygon(verts, facecolor="none", edgecolor=c)
          ax.add_patch(p)

    plt.title(caption)
    plt.imshow(np_image)
    plt.axis('off')
    plt.show()


def add_res(results, ax, color='green'):
    #for tt in results.values():
    if True:
        bboxes = results['boxes']
        labels = results['labels']
        scores = results['scores']
        #keep = scores >= 0.0
        #bboxes = bboxes[keep].tolist()
        #labels = labels[keep].tolist()
        #scores = scores[keep].tolist()
    #print(torchvision.ops.box_iou(tt['boxes'].cpu().detach(), torch.as_tensor([[xmin, ymin, xmax, ymax]])))
    
    colors = ['purple', 'yellow', 'red', 'green', 'orange', 'pink']
    
    for i, (b, ll, ss) in enumerate(zip(bboxes, labels, scores)):
        ax.add_patch(plt.Rectangle((b[0], b[1]), b[2] - b[0], b[3] - b[1], fill=False, color=colors[i], linewidth=3))
        cls_name = ll if isinstance(ll,str) else CLASSES[ll]
        text = f'{cls_name}: {ss:.2f}'
        print(text)
        ax.text(b[0], b[1], text, fontsize=15, bbox=dict(facecolor='white', alpha=0.8))

def plot_inference(im, caption):
  # mean-std normalize the input image (batch-size: 1)
  img = transform(im).unsqueeze(0).cuda()

  # propagate through the model
  memory_cache = model(img, [caption], encode_and_save=True)
  outputs = model(img, [caption], encode_and_save=False, memory_cache=memory_cache)
  #import ipdb;ipdb.set_trace()

  # keep only predictions with 0.7+ confidence
  probas = 1 - outputs['pred_logits'].softmax(-1)[0, :, -1].cpu()
  keep = (probas > 0.1).cpu()

  # convert boxes from [0; 1] to image scales
  bboxes_scaled = rescale_bboxes(outputs['pred_boxes'].cpu()[0, keep], im.size)
  # Extract the text spans predicted by each box
  positive_tokens = (outputs["pred_logits"].cpu()[0, keep].softmax(-1) > 0.1).nonzero().tolist()
  predicted_spans = defaultdict(str)

  for tok in positive_tokens:
    item, pos = tok
    if pos < 255:
        span = memory_cache["tokenized"].token_to_chars(0, pos)
        predicted_spans [item] += " " + caption[span.start:span.end]

  labels = [predicted_spans [k] for k in sorted(list(predicted_spans .keys()))]
  plot_results(im, probas[keep], bboxes_scaled, labels,caption)
  
deal_num = 0
total_time = 0

def DefaultDeal(data):
    return 'data received'.encode('utf-8')

def StartService(deal_func=DefaultDeal, port=4414):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('127.0.0.1', port))
        s.listen(5)
    except socket.error as msg:
        print(msg)
        sys.exit(1)

    while True:
        conn, addr = s.accept()
        _thread.start_new_thread(AcceptFunc, (conn, addr, deal_func))
def normalize_image(cfg, image):
        # image: CHW format
        pixel_mean = np.array(cfg.MODEL.PIXEL_MEAN).reshape(-1, 1, 1)
        pixel_std = np.array(cfg.MODEL.PIXEL_STD).reshape(-1, 1, 1)
        return (image - pixel_mean) / pixel_std
        
def AcceptFunc(conn, addr, deal_func):
    global deal_num
    global total_time
    while True:
        pkt_count = 0
        data = bytes()
        pkt_size = conn.recv(4)
        pkt_size = int.from_bytes(pkt_size, "big")
        print(pkt_size)
        try:
            while True:
                data += conn.recv(1024 * 1024 * 128)#1024 * 1024 * 128
                if data == 'exit'.encode('utf-8') or not data:
                    print('Connection closed with {}'.format(addr))
                    conn.close()
                    return
                pkt_count += 1
                if len(data) < pkt_size:
                    continue
                print('receive data from {}'.format(addr))
                print('pkt num is {}'.format(pkt_count))
                import time
                begin_time = time.time()
                result = deal_func(data)
                deal_num += 1
                total_time += time.time() - begin_time
                print('average process time', total_time / deal_num)
                conn.send(result)
                break
        except Exception as err:
            print(err)
            print(err.__traceback__)
def resizeimgtoyolov4format(desired_size,img):
    desired_size = 640
    # im = cv2.imread(im_pth)
    old_size = img.shape[:2] # old_size is in (height, width) format
    ratio = float(desired_size)/max(old_size)
    new_size = tuple([int(x*ratio) for x in old_size])
    # new_size should be in (width, height) format
    img = cv2.resize(img, (new_size[1], new_size[0]))

    delta_w = desired_size - new_size[1]
    delta_h = desired_size - new_size[0]
    top, bottom = delta_h//2, delta_h-(delta_h//2)
    left, right = delta_w//2, delta_w-(delta_w//2)

    color = [114, 114, 114]
    new_img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT,
        value=color)
    return new_img
def int_bbox_transformation(nms_out):
    nms_out[:,:4]=nms_out[:,:4].astype(int)
    return nms_out
def write_bbox_result(image,bbox,result_path):
    for i in range(len(bbox)):
        cv2.rectangle(image, (int(bbox[i][0]), int(bbox[i][1])), (int(bbox[i][2]), int(bbox[i][3])), (0,0,255), 2)
        cv2.putText(image, "{}".format(int(bbox[i][5])), (int(bbox[i][0]), int(bbox[i][1])), cv2.FONT_HERSHEY_SIMPLEX,  1, (0, 0, 255), 1, cv2.LINE_AA)
    output_path=os.path.join(result_path, "{}.jpg".format(datetime.datetime.now()))
    cv2.imwrite(output_path, image)

def postprocessbrick67(nms_out): 
    temp=[]
    brick6_list=[];brick7_list=[]
    for i in range(len(nms_out)):
        if nms_out[i,5]==5:
            brick6_list.append(nms_out[i])
    for i in range(len(nms_out)):
        if nms_out[i,5]==6:
            brick7_list.append(nms_out[i])
    #import ipdb;ipdb.set_trace()

    if len(brick6_list) and len(brick7_list)==0:
        print("doesnt find obj 6 and 7")
        return None
    if len(brick6_list)==1 and len(brick7_list)==1:
        if np.linalg.norm(brick6_list[0][:4]-brick7_list[0][:4],ord=2)<10:
            temp.append(brick6_list[0])
            temp.append(brick7_list[0])
        elif brick6_list[0][4]>brick7_list[0][4]:
            temp.append(brick6_list[0])
            print("doesnt find obj 7")
        else:
            temp.append(brick7_list[0])
            print("doesnt find obj 6")
        return temp


    if len(brick6_list)>=2 and len(brick7_list)>=2:
        #import ipdb;ipdb.set_trace()
        # if np.linalg.norm(brick6_list[0][:4]-brick7_list[0][:4],ord=2)>10:
            # temp.append(brick6_list[0])
            # temp.append(brick7_list[0])
        if brick6_list[0][4]>brick7_list[0][4]:
            temp.append(brick6_list[0])
            if np.linalg.norm(brick6_list[0][:4]-brick7_list[1][:4],ord=2)<10:
                temp.append(brick7_list[0])
            else:
                temp.append(brick7_list[1])

        else:
            temp.append(brick7_list[0])
            if np.linalg.norm(brick7_list[0][:4]-brick6_list[1][:4],ord=2)<10:
                temp.append(brick6_list[0])
            else:
                temp.append(brick6_list[1])
        #print(temp)
        return temp
def getbestbbox_eachobj(nms_out):
    result=[]
    for obj in range(5):
        temp=[]
        for i in range(len(nms_out)):
            if nms_out[i,5]==obj:
                temp.append(nms_out[i])
        try:
            result.append(temp[0])
        except:
            print("doesnt find obj{}".format(obj+1))
    brick67=postprocessbrick67(nms_out)
    result=np.array(result) 
    if brick67 is not None:
        finalresult=np.vstack((result,brick67))
        #import ipdb;ipdb.set_trace()

        return finalresult
    else:
        #import ipdb;ipdb.set_trace()
        return result
def getbestbbox_eachobj_tetris(nms_out):
    result=[]
    for obj in range(7+1):
        temp=[]
        for i in range(len(nms_out)):
            if nms_out[i,5]==obj:
                temp.append(nms_out[i])
        try:
            result.append(temp[0])
        except:
            import random
            print("doesnt find obj{}".format(obj))
            #result.append([random.random(),random.random(),random.random()*100,random.random()*100,0.0001,obj])
            result.append([0,0,50,50,0,obj+8])
    #brick67=postprocessbrick67(nms_out)
    result=np.array(result,dtype=np.float32) 
    # if brick67 is not None:
    #     finalresult=np.vstack((result,brick67))
    #     #import ipdb;ipdb.set_trace()

    #     return finalresult
    # else:
    #     #import ipdb;ipdb.set_trace()
    #     return result
    #import ipdb;ipdb.set_trace()
    return result
def IOU(box1, box2):
	""" We assume that the box follows the format:
		box1 = [x1,y1,x2,y2], and box2 = [x3,y3,x4,y4],
		where (x1,y1) and (x3,y3) represent the top left coordinate,
		and (x2,y2) and (x4,y4) represent the bottom right coordinate """
	x1, y1, x2, y2 = box1	
	x3, y3, x4, y4 = box2
	x_inter1 = max(x1, x3)
	y_inter1 = max(y1, y3)
	x_inter2 = min(x2, x4)
	y_inter2 = min(y2, y4)
	width_inter = abs(x_inter2 - x_inter1)
	height_inter = abs(y_inter2 - y_inter1)
	area_inter = width_inter * height_inter
	width_box1 = abs(x2 - x1)
	height_box1 = abs(y2 - y1)
	width_box2 = abs(x4 - x3)
	height_box2 = abs(y4 - y3)
	area_box1 = width_box1 * height_box1
	area_box2 = width_box2 * height_box2
	area_union = area_box1 + area_box2 - area_inter
	iou = area_inter / area_union
	return iou

def nms(boxes, conf_threshold=0.7, iou_threshold=0.4):
    bbox_list_thresholded = []
    bbox_list_new = []
    
    boxes_sorted = sorted(boxes, reverse=True, key = lambda x : x[5])
    
    for box in boxes_sorted:
        if box[5] > conf_threshold:
            bbox_list_thresholded.append(box)
        else:
            pass
    while len(bbox_list_thresholded) > 0:
        current_box = bbox_list_thresholded.pop(0)
        bbox_list_new.append(current_box)
        for box in bbox_list_thresholded:
            if current_box[4] == box[4]:
                iou = IOU(current_box[:4], box[:4])
                if iou > iou_threshold:
                    bbox_list_thresholded.remove(box)
    return bbox_list_new
def texts2label(texts):
    labels=[]
    for i in texts:
        if i.find("pink")!=-1 or i.find("L shape")!=-1:
            labels.append(1)
        elif i.find("dark blue")!=-1 or i.find("square")!=-1:
            labels.append(2)
        elif i.find("turquoise")!=-1 or i.find("U shape")!=-1:
            labels.append(3)   
        elif i.find("red")!=-1 or i.find("long cuboid")!=-1:
            labels.append(4)  
        elif i.find("purple")!=-1 or i.find("short cuboid")!=-1:
            labels.append(5)            
        elif i.find("aquamarine")!=-1 or i.find("Z shape")!=-1:
            labels.append(6)     
        elif i.find("yellow")!=-1 or i.find("short L shape")!=-1:
            labels.append(7)
        elif i.find("white")!=-1 or i.find("T shape")!=-1:
            labels.append(8)    
    return labels   
def get_i_b_box(output,text):
    "return classic bbox output with the order 1 insert 2 base"
    #import ipdb;ipdb.set_trace()
    find_list=[]
    obj_list=output[:,4]
    for i in obj_list:
        block=block_list[int(i)-1]
        find_list.append(text.find(block.color))
        find_list.append(text.find(block.shape))
    find_list=np.array(find_list).reshape(2,2)
    find_list_index = np.max(find_list,axis=1) #find the attribute index in text for each object
    b_i_index = np.argsort(find_list_index) # [1,0]##
    return output[b_i_index] 
if __name__ == '__main__':
    StartService()
