from sre_constants import SUCCESS
import time
import datetime
# Server use
from mdetr_server_utils.mdetr_server_utils import StartService
from mdetr_server_utils.mdetr_server_utils import nms,texts2label,get_i_b_box
import random
import torch
from PIL import Image
import requests
import torchvision.transforms as T
import matplotlib.pyplot as plt
from collections import defaultdict
import torch.nn.functional as F
import numpy as np
from skimage.measure import find_contours
import random
from matplotlib import patches,  lines
from matplotlib.patches import Polygon
from models import build_model
from models.postprocessors import build_postprocessors
from mmcv import Config
import cv2
from loguru import logger as loguru_logger

mdetr_process_time = 0
img_count = 0

#### server use ####
def GetBbox(img, text):
    global mdetr_process_time
    global img_count
    img_count += 1
    start_time = time.time()

    scores,boxes,labels = plot_inference(img, text )
    # import ipdb;ipdb.set_trace()
    mdetr_process_time += time.time() - start_time

    end_time=time.time()
 
    #import ipdb;ipdb.set_trace()
 
    # obj_labels=[]
    # for i in range(len(labels)):
    #     obj_labels.append(2)
    # obj_labels=np.array(obj_labels)
    
    obj_labels=texts2label(labels)

    nms_input  = np.hstack((boxes,np.array(obj_labels).reshape(-1,1),scores.numpy().reshape(-1,1)))

    # import ipdb;ipdb.set_trace()

    if obj_labels[0]==obj_labels[1]:
        i_b_bbox=get_i_b_box(nms_input,text)

    else:
        output = np.array(nms(nms_input))
        i_b_bbox=get_i_b_box(output,text)

    print('mdetr avg process time', mdetr_process_time/img_count)

    return i_b_bbox
def ReconstructJpg(b_arr):
    b_arr = np.frombuffer(b_arr, np.uint8)
    img = cv2.imdecode(b_arr, cv2.IMREAD_COLOR)
    return img

def DealFunc(data, save_img=False):
    begin_time = time.time()
    img_re = ReconstructJpg(data)
    print('bytes to img reconstruct time: {}'.format(time.time() - begin_time))
    im_rgb=cv2.cvtColor(img_re,cv2.COLOR_BGR2RGB)
    img = Image.fromarray(im_rgb).convert('RGB')
    fail=True
    while(fail):
        try:
            print("please type the aim text")
            text=input()
 
             # #red above
            # texts=[
            #     "Put the red brick above another red brick.",
            #     "Stack another same red block on it.",
            #     "Put another same red brick above it.",
            #     "Stack the long cuboid brick on another red brick.",
            #     "Assemble the long cuboid brick above another red brick.",
            #     "Put another same red object on it.",
            #     "Put another same red block above it.",
            #     "Assemble another same red brick above it.",
            #     "Assemble another same red block above it.",  
            #     "Put the red object above another one.",
            # ]
 
 
 
 
            
            #red right
            # texts=[    
            #         "Assemble another same red brick on the right of it.",
            #        "Assemble the red object on the right of another one.",
            #        "Assemble the red brick on the right of another one.",
            #        "Assemble the red object on the right of another red object.",
            #        "Assemble the red block on the right of another red block.",
            #        "Assemble the long cuboid object on the right of another red object.",
            #        "Assemble another same red block on the right of it.",
            # ]
            #red left
            # texts=[   
            #        "Assemble another same red brick on the left of it.",
            #        "Assemble the red object on the left of another one.",
            #        "Assemble the red brick on the left of another one.",
            #        "Assemble the red object on the left of another red object.",
            #        "Assemble the red block on the left of another red block.",
            #        "Assemble the long cuboid object on the left of another red object.",
            #        "Assemble another same red block on the left of it.",
            # ]

            # # #red front
            # texts=[  
            # # "Assemble the red block in front of another long cuboid block.",
            #  "Assemble the long cuboid object in front of another red object.",
            # #  "Assemble another same red brick in front of it.",
            # #   "Assemble the red brick in front of another one.",
            #   "Assemble the red brick in front of another long cuboid brick.",
            #   "Assemble the long cuboid brick in front of another red brick.",
            # #   "Assemble the red object in front of another one.",
            # #   "Assemble another same red block in front of it.",
            #   "Assemble the red brick in front of another long cuboid brick.",
              
            # ]





            # #red behind
            # texts=[  
            #     "Assemble the red block behind another long cuboid block.",
            #     "Assemble another same red object behind it.",
            #     "Assemble the long cuboid block behind another red block.",         
            #     "Assemble the red brick behind another red brick.",
            #     "Assemble another same red brick behind it.",
            #     "Assemble the red brick behind another one.",           
            #     "Assemble another same red block behind it.",
            #     "Assemble the red block behind another red block.",
            # ]



            # # #purple behind
            # texts=[                       
            #         "Assemble the purple block behind another purple block.",
            #         "Assemble the purple brick behind another one.",
            #         "Assemble the purple brick behind another short cuboid brick.",
            #         "Assemble the purple block behind another one.",
            #         "Assemble the short cuboid block behind another purple block.",
            #         "Assemble the purple object behind another one.",
            # ]

            
            # # #purple front
            # texts=[                       
            #     #    "Assemble the purple object in front of another one.",
            #     #    "Assemble the purple object in front of another purple object.",
            #     #    "Assemble the purple block in front of another purple block.",
            #     #    "Assemble the short cuboid brick in front of another purple brick.",
            #        "Assemble another same purple object in front of it.",
            #     #    "Assemble the purple brick in front of another one.",
            # ]
            
            
            
            # # #purple left
            # texts=[           
            # # "Assemble the short cuboid object on the left of another purple object.",
            # "Assemble another same purple brick on the left of it.",
            # #"Assemble the purple object on the left of another purple object.",
            # "Assemble the purple block on the left of another one.",
            # "Assemble the purple brick on the left of another purple brick.",
            # "Assemble another same purple block on the left of it.",
            #  "Assemble the purple object on the left of another one.",
            # ]
            # purple right
            # texts=[
            #     "Assemble another same purple brick on the right of it.",
            #     "Assemble the purple block on the right of another one.",
            #     "Assemble the purple block on the right of another short cuboid block.",
            #     "Assemble the purple object on the right of another short cuboid object.",
            #     "Assemble the short cuboid brick on the right of another purple brick.",
            #     "Assemble the purple brick on the right of another one.",
            #     "Assemble the purple block on the right of another purple block.",
            #     "Assemble another same purple block on the right of it.",
            #         ]        
            # #purple above
            # texts=[
            #     "Put the purple brick above another purple brick.",
            #     "Stack another same purple block on it.",
            #     "Put another same purple brick above it.",
            #     "Stack the short cuboid brick on another purple brick.",
            #     "Assemble the short cuboid brick above another purple brick.",
            #     "Put another same purple object on it.",
            #     "Put another same purple block above it.",
            #     "Assemble another same purple brick above it.",
            #     "Assemble another same purple block above it.",  
            #     "Put the purple object above another one.",
            # ]
            
            # ##blue front 
            # texts=[
            #     "Assemble the dark blue brick in front of another one.",
            #     "Assemble the square brick in front of another dark blue brick.",
            #     "Assemble the dark blue object in front of another square object.",
            #     "Assemble the dark blue block in front of another dark blue block.",
            #     "Assemble another same dark blue brick in front of it.",
            #     "Assemble the dark blue brick in front of another square brick.",
                
            # ]




            # ###blue behind 
            # texts=[
            #     # "Assemble the dark blue object behind another dark blue object.",
            #     # "Assemble another same dark blue brick behind it.",
            #     # "Assemble the square block behind another dark blue block.",
            #     # "Assemble the dark blue object behind another square object.",
            #     # "Assemble the square brick behind another dark blue brick.",
            #     # "Assemble the dark blue block behind another dark blue block.",
            #     "Assemble the dark blue brick behind another one.",
            #     # "Assemble another same dark blue block behind it.",
            # ]            
            # # ###blue left 
            # texts=[
            #     "Assemble the dark blue block on the left of another one.",
            #     "Assemble another same dark blue brick on the left of it.",
            #     "Assemble the dark blue brick on the left of another one.",
            #     "Assemble the dark blue brick on the left of another dark blue brick.",
            #     "Assemble another same dark blue object on the left of it.",
            #     "Assemble the dark blue object on the left of another square object.",
            #     "Assemble the dark blue block on the left of another square block.",
            #     "Assemble the dark blue block on the left of another dark blue block.",
            #     "Assemble the dark blue object on the left of another square object.",
            # ]

            ###blue right 
            # texts=[
            #     "Assemble the square object on the right of another dark blue object.",
            #     "Assemble the dark blue brick on the right of another square brick.",
            #     "Assemble the dark blue block on the right of another one.",
            #     "Assemble another same dark blue object on the right of it.",
            #     "Assemble the dark blue brick on the right of another square brick.",
            #     "Assemble the dark blue brick on the right of another dark blue brick.",
            #     "Assemble another same dark blue block behind it.",
            #      "Assemble the square brick on the right of another dark blue brick.",
            # ]

            # ###blue above 
            # texts=[
            #     "Assemble the dark blue brick above another square brick.",
            #     "Stack the dark blue object on another dark blue object.",
            #     "Assemble the dark blue brick above another dark blue brick.",
            #     "Put another same dark blue brick above it.",
            #     "Assemble the dark blue block above another dark blue block.",
            #     "Stack the square block on another dark blue block.",
            #     "Put the dark blue brick on another one.",
            #     "Stack the dark blue block on another one.",
            # ]
            # # yellow block
            # texts=[
            #     # "Insert another same yellow brick to it.",
            #     #    "Put the yellow brick together with another yellow brick.",
            #     #    "Insert another same yellow brick to it.",
            #        "Combine the yellow block with another short L shape block.",
            #     #    "Insert another same yellow block to it.",
            #     #    "Assemble another same yellow object to it.",
            #        "Assemble the short L shape brick to another yellow brick.",
            #     #    "Combine the yellow object with another yellow object.",
            #     #    "Put another same yellow object together with it.",
            #     #    "Put the yellow object together with another one.",
            #     #    "Insert the yellow object to another yellow object.",
            #     #    "Put the yellow brick together with another yellow brick.",
            #        "Combine the short L shape block with another yellow block.",
            #     #    "Insert the yellow brick to another yellow brick.",
            #     #    "Insert the yellow brick to another short L shape brick.",
            #        "Insert the short L shape block to another yellow block.",
            #     #    "Assemble the yellow block to another short L shape block.",
            #     #    "Assemble the yellow block to another one.",
            #     #    "Insert the yellow brick to another short L shape brick.",
                   
            #        ]
            
            
            
            # L shape
            # texts=[
            #     # "Assemble the L shape brick to another L shape brick.",
            #        "Insert the L shape block to another one.",
            #        "Assemble the pink object to another L shape object.",
            #         "Assemble the L shape block to another pink block.",
            #         "Combine the L shape brick with another L shape brick.",
            #         "Combine the L shape brick with another pink brick.",
            #         "Combine another same L shape block with it.",
            #         # "Put the pink object together with another L shape object.",
            #         "Combine another same pink object with it.",
            #         "Combine the pink object with another one.",
            #         # "Insert the pink brick to another one.",
            #         # "Put another same pink block together with it.",
            #         # "Put the pink object together with another one.",
            #         # "Assemble the pink brick to another one.",
            #         # "Assemble the pink brick to another L shape brick.",
            # ]
        
        
            
            # 8 insert 3
            # texts = [
            #         # "Insert the U shape block to the T shape block.",
            #         # "Grab the U shape block and combine it with the T shape block.",
            #         # "Assemble the U shape brick to the T shape brick.",
            #         # "Assemble the U shape block to the T shape block.",
            #         # "Grab the U shape brick and combine it with the white brick.",
            #         # "Insert the U shape object to the white object.",
            #         # "Grasp the U shape block and put it together with the white block.",
            #         # "Grasp the U shape brick and assemble it to the white brick.",
            #         "Grasp the U shape block and assemble it to the white brick.",
            #         # "Put the U shape block together with the white block.",
            #         # "Put the U shape block together with the T shape block.",
            # ]
            # 3 insert 8
            # texts = [
            #         #"Insert the T shape block to the U shape block.",
            #         #  "Insert the white block to the U shape block.",
            #         #  "Put the white brick together with the U shape brick.",
            #         #  "Grasp the T shape block and assemble it to the U shape block.",
            #         #  "Combine the T shape brick with the U shape brick.",
            #         #  "Grasp the T shape object and insert it to the turquoise object.",
            #         #  "Combine the T shape brick with the turquoise brick.",
            #         #  "Put the T shape brick together with the U shape brick.",
            #         #  "Grasp the T shape brick and insert it to the turquoise brick.",
            #         #  "Grasp the T shape brick and insert it to the U shape brick.",
            #         #  "Put the T shape block together with the U shape block.",
            #          ]            

            
            
            
            
            
            # text = "Assemble the pink block to another one."
            
            # text = "Assemble the yellow block to another one."
            # text = "red blocks."
            # text = texts[random.randint(0,len(texts))]
            with open("/home/bbnc/object_pose_benchmark/kuka_nlp/result/text.txt","w") as f: 
                f.write(text)
            i_b_bbox = GetBbox(img, text) # N*6
            i_b_bbox = i_b_bbox.astype(np.float32)
            print(i_b_bbox)
            fail=False
        except:
            print("process fail")
    # print(i_b_bbox.shape)

    # if save_img:
    #     bbox=int_bbox_transformation(bbox)
    #     write_bbox_result(img,bbox,"/home/bbnc/object_pose_benchmark")
    if i_b_bbox is None:
        print('no object detected')
        return 'no'.encode('utf-8')
    else:
        #import ipdb;ipdb.set_trace()
        return i_b_bbox.reshape(-1).tobytes() # bbox (N*6,)
## for block attribute extrace
class Block():
    def __init__(self,color,shape,name,size_x,size_y,size_z):
        self.color = color
        self.shape = shape
        self.name = name
        self.size_x = size_x
        self.size_y = size_y
        self.size_z = size_z
    def get_attr(self):
        return [self.color,self.shape][random.randint(0,1)]

obj1=Block("pink","L shape","obj1",None,None,None)
obj2=Block("dark blue","square","obj2",20,10,20)
obj3=Block("turquoise","U shape","obj3",None,None,None)
obj4=Block("red","long cuboid","obj4",40,10,10)
obj5=Block("purple","short cuboid","obj5",20,10,10)
obj6=Block("aquamarine","Z shape","obj6",None,None,None)
obj7=Block("yellow","short L shape","obj7",None,None,None)
obj8=Block("white","T shape","obj8",None,None,None)
block_list=[obj1,obj2,obj3,obj4,obj5,obj6,obj7,obj8]

### mdetr use


torch.set_grad_enabled(False)

# standard PyTorch mean-std input image normalization
transform = T.Compose([
    T.Resize(800),
    T.ToTensor(),
    T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
])

# for output bounding box post-processing
def box_cxcywh_to_xyxy(x):
    x_c, y_c, w, h = x.unbind(1)
    b = [(x_c - 0.5 * w), (y_c - 0.5 * h),
         (x_c + 0.5 * w), (y_c + 0.5 * h)]
    return torch.stack(b, dim=1)

def rescale_bboxes(out_bbox, size):
    img_w, img_h = size
    b = box_cxcywh_to_xyxy(out_bbox)
    b = b * torch.tensor([img_w, img_h, img_w, img_h], dtype=torch.float32)
    return b

# colors for visualization
COLORS = [[0.000, 0.447, 0.741], [0.850, 0.325, 0.098], [0.929, 0.694, 0.125],
          [0.494, 0.184, 0.556], [0.466, 0.674, 0.188], [0.301, 0.745, 0.933]]

def apply_mask(image, mask, color, alpha=0.5):
    """Apply the given mask to the image.
    """
    for c in range(3):
        image[:, :, c] = np.where(mask == 1,
                                  image[:, :, c] *
                                  (1 - alpha) + alpha * color[c] * 255,
                                  image[:, :, c])
    return image

def plot_results(pil_img, scores, boxes, labels,caption, masks=None):
    plt.figure(figsize=(16,10))
    np_image = np.array(pil_img)
    ax = plt.gca()
    colors = COLORS * 100

    if masks is None:
      masks = [None for _ in range(len(scores))]
    assert len(scores) == len(boxes) == len(labels) == len(masks)
    for s, (xmin, ymin, xmax, ymax), l, mask, c in zip(scores, boxes.tolist(), labels, masks, colors):
        ax.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
                                   fill=False, color=c, linewidth=3))
        text = f'{l}: {s:0.2f}'
        ax.text(xmin, ymin, text, fontsize=15, bbox=dict(facecolor='white', alpha=0.8))

        if mask is None:
          continue
        np_image = apply_mask(np_image, mask, c)

        padded_mask = np.zeros((mask.shape[0] + 2, mask.shape[1] + 2), dtype=np.uint8)
        padded_mask[1:-1, 1:-1] = mask
        contours = find_contours(padded_mask, 0.5)
        for verts in contours:
          # Subtract the padding and flip (y, x) to (x, y)
          verts = np.fliplr(verts) - 1
          p = Polygon(verts, facecolor="none", edgecolor=c)
          ax.add_patch(p)
    # img=Image.fromarray(np_image)
    # img.save("result.png")
    plt.imshow(np_image)

    plt.title(caption)
    plt.axis('off')
    save_img=True
    if save_img==True:
        plt.savefig("/home/bbnc/object_pose_benchmark/kuka_nlp/result/realtime_bbox.png")
    return scores,boxes,labels
    # plt.show()

def plot_results_no_text(pil_img, scores, boxes, labels,caption, masks=None):
    plt.figure(figsize=(16,10))
    np_image = np.array(pil_img)
    ax = plt.gca()
    colors = COLORS * 100

    if masks is None:
      masks = [None for _ in range(len(scores))]
    assert len(scores) == len(boxes) == len(labels) == len(masks)
    for s, (xmin, ymin, xmax, ymax), l, mask, c in zip(scores, boxes.tolist(), labels, masks, colors):
        ax.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin,
                                   fill=False, color=c, linewidth=3))
        # text = f'{l}: {s:0.2f}'
        # ax.text(xmin, ymin, text, fontsize=15, bbox=dict(facecolor='white', alpha=0.8))

        if mask is None:
          continue
        np_image = apply_mask(np_image, mask, c)

        padded_mask = np.zeros((mask.shape[0] + 2, mask.shape[1] + 2), dtype=np.uint8)
        padded_mask[1:-1, 1:-1] = mask
        contours = find_contours(padded_mask, 0.5)
        for verts in contours:
          # Subtract the padding and flip (y, x) to (x, y)
          verts = np.fliplr(verts) - 1
          p = Polygon(verts, facecolor="none", edgecolor=c)
          ax.add_patch(p)
    # img=Image.fromarray(np_image)
    # img.save("result.png")
    plt.imshow(np_image)

    plt.title(caption)
    plt.axis('off')
    save_img=True
    if save_img==True:
        plt.savefig("/home/bbnc/object_pose_benchmark/kuka_nlp/result/realtime_bbox_no_text.png")
    # plt.show()
def add_res(results, ax, color='green'):
    #for tt in results.values():
    if True:
        bboxes = results['boxes']
        labels = results['labels']
        scores = results['scores']
        #keep = scores >= 0.0
        #bboxes = bboxes[keep].tolist()
        #labels = labels[keep].tolist()
        #scores = scores[keep].tolist()
    #print(torchvision.ops.box_iou(tt['boxes'].cpu().detach(), torch.as_tensor([[xmin, ymin, xmax, ymax]])))
    
    colors = ['purple', 'yellow', 'red', 'green', 'orange', 'pink']
    
    for i, (b, ll, ss) in enumerate(zip(bboxes, labels, scores)):
        ax.add_patch(plt.Rectangle((b[0], b[1]), b[2] - b[0], b[3] - b[1], fill=False, color=colors[i], linewidth=3))
        cls_name = ll if isinstance(ll,str) else CLASSES[ll]
        text = f'{cls_name}: {ss:.2f}'
        print(text)
        ax.text(b[0], b[1], text, fontsize=15, bbox=dict(facecolor='white', alpha=0.8))

def plot_inference(im, caption):
  # mean-std normalize the input image (batch-size: 1)
  img = transform(im).unsqueeze(0).cuda()

  # propagate through the model
  with torch.no_grad():
        memory_cache = model(img, [caption], encode_and_save=True)
        outputs = model(img, [caption], encode_and_save=False, memory_cache=memory_cache)

  # keep only predictions with 0.7+ confidence
  probas = 1 - outputs['pred_logits'].softmax(-1)[0, :, -1].cpu()
  keep = (probas > 0.7).cpu()

  # convert boxes from [0; 1] to image scales
  bboxes_scaled = rescale_bboxes(outputs['pred_boxes'].cpu()[0, keep], im.size)

#   bboxes_scaled[:,1]=bboxes_scaled[:,1]-15
  
#   bboxes_scaled[:,0]=bboxes_scaled[:,0]+10
#   bboxes_scaled[:,3]=bboxes_scaled[:,3]+15
#   bboxes_scaled[0,3]=bboxes_scaled[0,3]+10
#   bboxes_scaled[1,3]=bboxes_scaled[1,3]+15
  # Extract the text spans predicted by each box
  positive_tokens = (outputs["pred_logits"].cpu()[0, keep].softmax(-1) > 0.11).nonzero().tolist()
  predicted_spans = defaultdict(str)
  for tok in positive_tokens:
    item, pos = tok
    
    if pos < 255:
        span = memory_cache["tokenized"].token_to_chars(0, pos)
        predicted_spans [item] += " " + caption[span.start:span.end]

  labels = [predicted_spans [k] for k in sorted(list(predicted_spans .keys()))]
  #import ipdb;ipdb.set_trace()
  for i in range(len(labels)):
        if bboxes_scaled[i,1]>400 or bboxes_scaled[i,3]>400:
            bboxes_scaled = np.delete(bboxes_scaled,i,0)
  scores,boxes,labels = plot_results(im, probas[keep], bboxes_scaled, labels,caption)
  plot_results_no_text(im, probas[keep], bboxes_scaled, labels,caption)
  return scores,boxes,labels


    
global model
path_RESNET101="/home/bbnc/mdetr/checkpoint/tetris_RESNET101/checkpoint0027.pth"# 'mdetr_resnet101_clevrref'
path_EB5="/home/bbnc/mdetr/checkpoint/tetris_EB5/BEST_checkpoint.pth"# "mdetr_efficientnetB5_clevrref"
path_EB3="/home/bbnc/mdetr/checkpoint/tetris_EB3/checkpoint0029.pth"# "mdetr_efficientnetB3_clevrref"
model, postprocessor = torch.hub.load('/home/bbnc/mdetr', "mdetr_resnet101_clevrref",source='local',path = path_RESNET101, return_postprocessor=True)
model = model.cuda()
model.eval()


print('MDETR tetris BBox service started')
StartService(DealFunc)