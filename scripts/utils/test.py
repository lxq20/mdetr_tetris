from text import get_root_and_nouns
import re
from typing import List, Tuple
from matplotlib.font_manager import json_load
import numpy as np
import json
from PIL import Image
from io import BytesIO
import base64
import tqdm
import nltk
import spacy
import cv2
nlp = spacy.load("en_core_web_sm")

nltk.download("stopwords")
from nltk.corpus import stopwords

STOP_WORDS = set(stopwords.words("english")) - set(["above", "below", "between", "further", "he", "she", "they"])


def consolidate_spans(spans: List[Tuple[int, int]], caption: str, rec=True):
    """Accepts a list of spans and the the corresponding caption.
    Returns a cleaned list of spans where:
        - Overlapping spans are merged
        - It is guaranteed that spans start and end on a word
    """
    sorted_spans = sorted(spans)
    cur_end = -1
    cur_beg = None
    final_spans: List[Tuple[int, int]] = []
    for s in sorted_spans:
        if s[0] >= cur_end:
            if cur_beg is not None:
                final_spans.append((cur_beg, cur_end))
            cur_beg = s[0]
        cur_end = max(cur_end, s[1])

    if cur_beg is not None:
        final_spans.append((cur_beg, cur_end))

    # Now clean the begining/end
    clean_spans: List[Tuple[int, int]] = []
    for s in final_spans:
        beg, end = s
        end = min(end, len(caption))
        while beg < len(caption) and not caption[beg].isalnum():
            beg += 1
        while end > 0 and not caption[end - 1].isalnum():
            end -= 1
        # Try to get hyphenated words
        if end < len(caption) and caption[end] == "-":
            # print("trigg")
            next_space = caption.find(" ", end)
            if next_space == -1:
                end = len(caption)
            else:
                end = next_space + 1
        if beg > 0 and caption[beg - 1] == "-":
            prev_space = caption.rfind(" ", 0, beg)
            if prev_space == -1:
                beg = 0
            else:
                beg = prev_space + 1
        if 0 <= beg < end <= len(caption):
            clean_spans.append((beg, end))
    if rec:
        return consolidate_spans(clean_spans, caption, False)
    return clean_spans


data=json_load("/home/bbnc/Storage/clevr/clevr_annotations/clevrref/val.json")
def get_pn_token(str):
    refexp=str
    root_text, negative_text, rt_spans, negative_spans = get_root_and_nouns(refexp)
    root_spans = consolidate_spans(rt_spans, refexp)#positive token
    neg_spans = consolidate_spans(negative_spans, refexp)#negative_token
    print("positive token\n{}\nnegative_token\n{}".format(root_spans,neg_spans))

    return root_spans,neg_spans
def get_img_instances_num_list():
    ins_list={} #anno_start anno_end count images_id
    count=1


    for idx in range(len(data['annotations'])-1):
        if data['annotations'][idx]['image_id']!=data['annotations'][idx+1]['image_id']:
            ins_list['{}'.format(data['annotations'][idx]['image_id'])]={"anno_start":idx-count+1,"anno_end":idx,"count":count}
            count=1
        else:
            count=count+1
    #for list item
    count=len(data['annotations'][-5:])
    idx_end=len(data['annotations'])-1
    idx_start=idx-count+1
    ins_list['{}'.format(data['annotations'][idx]['image_id'])]={"anno_start":idx_start,"anno_end":idx_end,"count":count}
    return ins_list

def get_isfinal_list(data):
    newanno=[]
    for idx in range(len(data['annotations'])):
        if data['annotations'][idx]['isfinal']==1:
            data['annotations'][idx]['anno_idx']=idx
            newanno.append(data['annotations'][idx])
    return newanno
import copy
mdata=copy.deepcopy(data)
ins_list=get_img_instances_num_list()
#id 0 = 698794

str=mdata['images'][4]['caption']
file_name=mdata['images'][4]['file_name']
positive_token, negative_token = get_pn_token(str)
mdata['images'][4]['tokens_negative'] = negative_token
path="/home/bbnc/Storage/clevr/CLEVR_v1.0/images/val/"+file_name
img=cv2.imread(path)
x1 = mdata['annotations'][21]['bbox'][0]
y1 = mdata['annotations'][21]['bbox'][1]
x2 = x1 + mdata['annotations'][21]['bbox'][2]
y2 = y1 + mdata['annotations'][21]['bbox'][3]

cv2.rectangle(img,(x1, y1),(x2,y2),(0,0,255),3)

result_path="/home/bbnc/mdetr/result.png"
cv2.imwrite(result_path, img)
new_anno=get_isfinal_list(data)
import ipdb;ipdb.set_trace()
