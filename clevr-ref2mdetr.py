from matplotlib.font_manager import json_load
import numpy as np
import json
from PIL import Image
from io import BytesIO
import base64
import tqdm
from os.path import exists

import cv2
data=json_load("/home/bbnc/Storage/clevr/clevr_annotations/clevrref/train.json")


        
def get_img_instances_num_list():
    ins_list={} #anno_start anno_end count images_id
    count=1


    for idx in range(len(data['annotations'])-1):
        if data['annotations'][idx]['image_id']!=data['annotations'][idx+1]['image_id']:
            ins_list['{}'.format(data['annotations'][idx]['image_id'])]={"anno_start":idx-count+1,"anno_end":idx,"count":count}
            count=1
        else:
            count=count+1
    #for list item
    count=len(data['annotations'][-5:])
    idx_end=len(data['annotations'])-1
    idx_start=idx-count+1
    ins_list['{}'.format(data['annotations'][idx]['image_id'])]={"anno_start":idx_start,"anno_end":idx_end,"count":count}
    return ins_list

def get_isfinal_list(data):
    newanno=[]
    for idx in range(len(data['annotations'])):
        if data['annotations'][idx]['isfinal']==1:
            newanno.append(data['annotations'][idx])
    return newanno
# ipdb> data.keys()
# dict_keys(['info', 'licenses', 'images', 'annotations', 'categories'])
#ins_list=get_img_instances_num_list()
# import ipdb;ipdb.set_trace()
new_anno=get_isfinal_list(data)
drawing=False
import csv
with open("clevr-ref+_train.json",'w', newline='', encoding='utf-8') as file:
    tsv_writer = csv.writer(file, delimiter='\t')
    
    for idx,i in enumerate(tqdm.tqdm(new_anno)):
        image_id=i["image_id"]
        file_name=data["images"][image_id-698794]['file_name']#-698794
        import ipdb;ipdb.set_trace()
        images_base64=img2base64(file_name)

        uni_id="{}_{}".format(file_name[10:-4],image_id)#10
        caption=data['images'][image_id-698794]['caption']#-698794
        bbox="{}".format(i['bbox'])[1:-1].replace(' ', '')

        tsv_writer.writerow([uni_id,image_id,caption,bbox,images_base64])#
        if drawing==True:
            path="/home/bbnc/Storage/clevr/CLEVR_v1.0/images/train/"+file_name
            writepath="/home/bbnc/OFA/test/"+uni_id+".jpg"
            if  exists(writepath)==False:
                img=cv2.imread(path)
                cv2.imwrite(writepath,img)

            img=cv2.imread(writepath)
            x1 = i['bbox'][0]
            y1 = i['bbox'][1]
            x2 = x1 + i['bbox'][2]
            y2 = y1 + i['bbox'][3]
            cv2.rectangle(img,(x1, y1),(x2,y2),(0,0,255),3)
            cv2.imwrite(writepath,img)
        if idx==500000:
            break
        
        # import ipdb;ipdb.set_trace()




     